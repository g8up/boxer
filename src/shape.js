import {
  setPosition,
} from './util.js';

const dies = new WeakMap();

export default class Shape {
  constructor({
    text,
  }) {
    this.text = text;
  }

  birth(fontBoundWidth) {
    if (dies.has(this.text)) {
      clearInterval()
    }
    this.stopToDie();
    setPosition(this.text, Math.random() * fontBoundWidth, 0);
    this.text.alpha =1;
  }

  isOld(stageHeight) {
    return this.text.y > stageHeight;
  }

  // 透明度降低
  die() {
    const timer = setInterval(() => {
      if (this.text.alpha === 0.1) {
        this.stopToDie(timer);
      }
      this.text.alpha -= 0.1;
    }, 100);
    dies.set(this.text, timer);
  }

  stopToDie(timer) {
    let t;
    if (dies.has(this.text)) {
      t = dies.get(this.text);
      dies.delete(this.text);
    }
    clearInterval(timer || t);
  }

  // 衰老
  senesce({
    step,
  }) {
    this.text.y += step;
  }
}