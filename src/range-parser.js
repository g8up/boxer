/**
 * 解析字母范围设定
 */
// case:
// abcd
// a-d
// a-d,e-f,h
const getItemInRange = (start, end) => {
  let charCodeStart = start.charCodeAt(0);
  let charCodeEnd = end.charCodeAt(0);
  let min = Math.min(charCodeStart, charCodeEnd);
  let max = Math.max(charCodeStart, charCodeEnd);
  let ret = [];
  for (let i = min; i <= max; i++) {
    ret.push(String.fromCharCode(i).toLowerCase());
  }
  return ret;
}

export default (rangeStr = '') => {
  if( typeof rangeStr !== 'string'){
    return [];
  }
  let ranges = rangeStr.toLowerCase().replace(/，/g,',').replace(/[^a-z,A-Z,\-,\,]/g, '').split(',');// 需参考 chrome 处理方案更新算法
  let ret = [];
  return ranges.map(range => {
    if (range.indexOf('-') === 1) {
      return getItemInRange.apply(null, range.split('-'));
    }
    else {
      return range.split('');
    }
  }).reduce((accumulator, a) => {
    return accumulator.concat(a);
  }, ret);
}

// test
// import parser from './range-parser.js'

// var ranges = 'abc，h-g,j-l,z'

// var ret = parser(ranges);

// console.log(ret);