import {
  say,
} from './util.js'

export default class Speaker {
  constructor({
    srcPrefix,
    /** 重复次数 */
    repeat,
  }) {
    this.srcPrefix = srcPrefix;
    this.repeat = repeat;
  }

  read(char) {
    const src = `${this.srcPrefix}/${char}.mp3`;
    say(src, this.repeat);
  }
}