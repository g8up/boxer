import rangeParser from './range-parser.js'
import {
  filterCharacter,
  uniqueArr,
} from './util.js'

export default class Range {

  constructor({
    /** 简略范围表达式 */
    shortPattern,
    /** 是否强制转换为大写 */
    forceUpperCase = false,
  }) {
    this.shortPattern = shortPattern;
    this.forceUpperCase = forceUpperCase;
  }

  /** 转换 range 为大写字母 */
  toUpperCase(range) {
    return range.map(char => char.toUpperCase());
  }

  toList() {
    let range = rangeParser(this.shortPattern);
    if (this.forceUpperCase) {
      range = this.toUpperCase(range);
    }
    return uniqueArr(filterCharacter(range)) || []; // 此处需要过滤重复
  }

}