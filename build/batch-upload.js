// test
// 上传 a.mp3,b.mp3,c.mp3 到七牛云
// npm run upload -- abc

var exec = require('child_process').exec;
var PREFIX = 'boxer';
const CDN_HOST = 'cdn.chromedevtools.com';

function run(command){
  return new Promise( function(resolve, reject){
    exec(command, function(err, stdout, stderr) {
      if (err) {
        reject();
        throw err;
      }
      else {
        console.log(stdout);
        resolve(command);
      }
    });
  })

}

function upload(file){
  return run(`cdn -f -p ${PREFIX} ${file} `)
}

function refresh(files) {
  return run(`cdn -r ${files.map(file=>`http://${CDN_HOST}/${PREFIX}/${file}`).join(',')}`)
}

var chars = process.argv[2] || '';
if( chars.length ){
  var files = (process.argv[2]).split('').map( char =>{
    return `${char}.mp3`;
  })

  if( files.length ){
    Promise.all(files.map(file=>{
      return upload( `audio/${file}` );
    })).then(function(values) {
      // console.log(values);
      refresh( files ).then(function(cmds) {
        console.log( cmds );
      });
    });
  }
}
