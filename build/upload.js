/**
 * 文件上传
 * 目录： dist/、src/
 */
const path = require('path');
const fs = require('fs');
const cdn = require('cdn/src/util.js');

const resolve = (dir) => {
  return path.resolve(__dirname, dir);
};

// 根据扩展名过滤指定目录下文件
const getDirByExt = (dir, ext) => {
  return fs.readdirSync(dir)
    .filter(file => path.extname(file) === `.${ext}`)
    .map( file => {
      return path.resolve( __dirname, dir, file); // full path
    });
};

const src = getDirByExt(resolve('../src'), 'js');
const dist = getDirByExt(resolve('../dist'), 'js');
const JS = src.concat(dist);
console.log( JS );

const prefix = 'boxer';
const isForce = true;

// upload
cdn.uploadFlow(JS, prefix, isForce);
