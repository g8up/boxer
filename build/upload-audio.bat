@usage: PROJECT_ROOT> build/upload-audio.bat

cdn -f src/audio/a.mp3 -p boxer/audio
cdn -f src/audio/b.mp3 -p boxer/audio
cdn -f src/audio/c.mp3 -p boxer/audio
cdn -f src/audio/d.mp3 -p boxer/audio
cdn -f src/audio/e.mp3 -p boxer/audio
cdn -f src/audio/f.mp3 -p boxer/audio
cdn -f src/audio/g.mp3 -p boxer/audio
cdn -f src/audio/h.mp3 -p boxer/audio
cdn -f src/audio/i.mp3 -p boxer/audio
cdn -f src/audio/j.mp3 -p boxer/audio
cdn -f src/audio/k.mp3 -p boxer/audio
cdn -f src/audio/l.mp3 -p boxer/audio
cdn -f src/audio/m.mp3 -p boxer/audio
cdn -f src/audio/n.mp3 -p boxer/audio
cdn -f src/audio/o.mp3 -p boxer/audio
cdn -f src/audio/p.mp3 -p boxer/audio
cdn -f src/audio/q.mp3 -p boxer/audio
cdn -f src/audio/r.mp3 -p boxer/audio
cdn -f src/audio/s.mp3 -p boxer/audio
cdn -f src/audio/t.mp3 -p boxer/audio
cdn -f src/audio/u.mp3 -p boxer/audio
cdn -f src/audio/v.mp3 -p boxer/audio
cdn -f src/audio/w.mp3 -p boxer/audio
cdn -f src/audio/x.mp3 -p boxer/audio
cdn -f src/audio/y.mp3 -p boxer/audio
cdn -f src/audio/z.mp3 -p boxer/audio

cdn -f dist/bundle.js -p boxer
cdn -f src/entry.js -p boxer
cdn -f src/util.js -p boxer
cdn -f src/range-parser.js -p boxer
cdn -f src/lib/are.js -p boxer

@refresh
cdn -r http://cdn.chromedevtools.com/boxer/bundle.js,http://cdn.chromedevtools.com/boxer/entry.js,http://cdn.chromedevtools.com/boxer/util.js,http://cdn.chromedevtools.com/boxer/range-parser.js