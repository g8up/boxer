# Boxer
> 小霸王打字游戏，适合3岁学龄儿童英语字母入门练习

# 功能
- 支持通过参数设置字母范围和同时出现的字母个数，如：
  https://g8up.gitee.io/boxer/#range=a-d,mn,x-z&upper=1&num=3&repeat=2

| 参数   | 描述      | 默认值 |
|-|-|-|
| range | 字母范围    | a-z |
| upper | 是否强制大写，1：是，0：否 | 0 |
| num | 同时出现的字母个数 | 5 |
| repeat | 重复朗读次数 | 1 |

# Release
- npm run build
- npm run upload

# Doc
- https://www.cnblogs.com/iamzhanglei/p/4435384.html
- https://github.com/dntzhang/cax

# Release
```
npm run release
```

# 搜搜
- https://www.baidu.com/s?wd=小霸王打字游戏