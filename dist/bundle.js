'use strict';

function getColor() {
  return '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
}

function setPosition(text, x, y) {
  text.x = x;
  text.y = y;
}

// 随机生成小写字母


/** 按照指定范围生成字母 */
function letterGenerator(charRange) {
  var len = charRange.length;
  return function () {
    var index = Math.floor(Math.random() * len);
    return charRange[index];
  }
}

// 随机大小写
const randomCase = (char) => {
  if (Math.random() > 0.5) {
    return char.toLowerCase();
  }
  return char.toUpperCase();
};

function speaker(src, cb) {
  var audio = document.createElement('audio');
  audio.autoplay = true;
  if (typeof cb === 'function') {
    audio.onended = cb;
  }
  audio.src = src;
}

const say = (src, repeat) => {
  if (repeat > 0) {
    speaker(src, () => {
      say(src, --repeat);
    }); // 重复读
  }
};

// 过滤非字母字符
const filterCharacter = (range) => {
  return range.filter(char => {
    return /^[a-z,A-Z]$/.test(char);
  });
};

const uniqueArr = (arr) => {
  return Array.from(new Set(arr));
};

const IteratorToObject = (it) => {
  const keys = it.keys();
  const ret = Object.create(null);
  for (let key of keys) {
    ret[key] = it.get(key);
  }
  return ret;
};

const getHashParams = (hash) => {
  if (hash && hash.length > 2) {
    hash = hash.slice(1);
  }
  const params = new URLSearchParams(hash);
  return IteratorToObject(params);
};

const getUrlParams = () => {
  const {
    hash,
    search,
  } = window.location;
  if (hash) {
    return getHashParams(hash);
  } else {
    return getHashParams(search); // 兼容 search
  }
};

const getClientSize = () => {
  var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  return {
    w,
    h
  };
};

/**
 * 解析字母范围设定
 */
// case:
// abcd
// a-d
// a-d,e-f,h
const getItemInRange = (start, end) => {
  let charCodeStart = start.charCodeAt(0);
  let charCodeEnd = end.charCodeAt(0);
  let min = Math.min(charCodeStart, charCodeEnd);
  let max = Math.max(charCodeStart, charCodeEnd);
  let ret = [];
  for (let i = min; i <= max; i++) {
    ret.push(String.fromCharCode(i).toLowerCase());
  }
  return ret;
};

var rangeParser = (rangeStr = '') => {
  if( typeof rangeStr !== 'string'){
    return [];
  }
  let ranges = rangeStr.toLowerCase().replace(/，/g,',').replace(/[^a-z,A-Z,\-,\,]/g, '').split(',');// 需参考 chrome 处理方案更新算法
  let ret = [];
  return ranges.map(range => {
    if (range.indexOf('-') === 1) {
      return getItemInRange.apply(null, range.split('-'));
    }
    else {
      return range.split('');
    }
  }).reduce((accumulator, a) => {
    return accumulator.concat(a);
  }, ret);
};

// test
// import parser from './range-parser.js'

// var ranges = 'abc，h-g,j-l,z'

// var ret = parser(ranges);

// console.log(ret);

class Range {

  constructor({
    /** 简略范围表达式 */
    shortPattern,
    /** 是否强制转换为大写 */
    forceUpperCase = false,
  }) {
    this.shortPattern = shortPattern;
    this.forceUpperCase = forceUpperCase;
  }

  /** 转换 range 为大写字母 */
  toUpperCase(range) {
    return range.map(char => char.toUpperCase());
  }

  toList() {
    let range = rangeParser(this.shortPattern);
    if (this.forceUpperCase) {
      range = this.toUpperCase(range);
    }
    return uniqueArr(filterCharacter(range)) || []; // 此处需要过滤重复
  }

}

const dies = new WeakMap();

class Shape {
  constructor({
    text,
  }) {
    this.text = text;
  }

  birth(fontBoundWidth) {
    if (dies.has(this.text)) {
      clearInterval();
    }
    this.stopToDie();
    setPosition(this.text, Math.random() * fontBoundWidth, 0);
    this.text.alpha =1;
  }

  isOld(stageHeight) {
    return this.text.y > stageHeight;
  }

  // 透明度降低
  die() {
    const timer = setInterval(() => {
      if (this.text.alpha === 0.1) {
        this.stopToDie(timer);
      }
      this.text.alpha -= 0.1;
    }, 100);
    dies.set(this.text, timer);
  }

  stopToDie(timer) {
    let t;
    if (dies.has(this.text)) {
      t = dies.get(this.text);
      dies.delete(this.text);
    }
    clearInterval(timer || t);
  }

  // 衰老
  senesce({
    step,
  }) {
    this.text.y += step;
  }
}

const isLive = {};
const Text = cax.Text;

class Queue {
  constructor({
    stage,
    queue = {},
    range,
    total,
    forceUpperCase = false,
    speaker,
  }) {
    this.stage = stage;
    this.queue = queue;
    this.range = range;
    this.total = total;
    this.forceUpperCase = forceUpperCase;
    this.speaker = speaker;

    this.charGen = letterGenerator(range);
  }

  getItem(id) {
    return this.queue[id];
  }


  getLiveLength() {
    return Object.keys(isLive).filter(id => isLive[id]).length;
  }

  isExist(id) {
    return !!this.getItem(id.toLowerCase()) || this.getItem(id.toUpperCase());
  }

  isExactExist(id) {
    return !!this.getItem(id);
  }

  add(fontBoundWidth) {
    const char = this.charGen();
    const id = this.forceUpperCase ? char : randomCase(char);
    isLive[id] = true; // 复活
    let shape;
    if (this.isExactExist(id)) {
      shape = this.queue[id];
    } else {
      const text = new Text(id, {
        font: '100px Georgia',
        color: getColor(),
      });

      text.on('click', () => {
        console.log(id);
        this.speaker.read(id);
        this.hide(id);
      });

      shape = new Shape({
        text,
      });
      this.queue[id] = shape;
      this.stage.add(shape.text);
    }
    shape.birth(fontBoundWidth);
  }

  hide(id) {
    const ids = Object.keys(isLive).filter(id => isLive[id]);
    const lower = id.toLowerCase();
    const upper = id.toUpperCase();
    [lower, upper].forEach(id => {
      if (ids.includes(id)) {
        isLive[id] = false;
        this.getItem(id).die();
      }
    });
  }

  senesce({
    step,
  }) {
    Object.keys(this.queue).forEach(id => {
      if (isLive[id]) {
        const shape = this.getItem(id);
        if (shape.isOld()) {
          isLive[id] = false;
          shape.die();
        } else {
          shape.senesce({
            step,
          });
        }
      }
    });
  }

  spawn(fontBoundWidth) {
    if (this.getLiveLength() < this.total) {
      this.add(fontBoundWidth);
    }
  }
}

class Speaker {
  constructor({
    srcPrefix,
    /** 重复次数 */
    repeat,
  }) {
    this.srcPrefix = srcPrefix;
    this.repeat = repeat;
  }

  read(char) {
    const src = `${this.srcPrefix}/${char}.mp3`;
    say(src, this.repeat);
  }
}

var OPTION = {
  age: 3,
  strict: true, // 严格模式：按错不发音
};

const defaultRange = 'a-z';
const defaultParams = {
  range: defaultRange,
  num: 3,
  repeat: 1,
};
var CONFIG = {
  3: {
    total: 3,
    step: .3,
    ratio: 0.01, // 击键成功次数与速度（step）之间的调整系数
    repeat: 1, // 朗读重复次数
  },
};
var config = CONFIG[OPTION.age];
var stage;
var stageHeight;
var fontSize = 100;
let {
  total = 10,
    step = 1,
    ratio,
    repeat,
} = config;
var fontBoundWidth;
let isPageVisible = document.hidden || false;

// 速度 = 成功次数 * 系数 + base step
function adjustSpeed(step, success, ratio) {
  return step + success * ratio;
}

(function () {
  const {
    w,
    h
  } = getClientSize();
  {
    var c = document.querySelector('#ourCanvas');
    c.setAttribute('width', w + 'px');
    c.setAttribute('height', h + 'px');
  }

  var Stage = cax.Stage;

  //webgl不支持渲染Text，你可以使用ARE.Label
  stage = new Stage(w, h, '#ourCanvas');
  // stage.debug = true;
  stageHeight = stage.height;
  fontBoundWidth = stage.width - fontSize;
  var FREQUENCY = 1000;
  var success = 0; // 击键成功次数

  function start(stage, FREQUENCY) {

    var setting = Object.assign({}, defaultParams, getUrlParams()); // 从 url 中获取配置
    console.log(setting);
    var rangeSetting = setting.range;
    var totalSetting = parseInt(setting.num || setting.total, 10); // 暂时兼容 `total`
    var repeatSetting = parseInt(setting.repeat, 10) || repeat;
    var group1 = 'asdfghjkl';

    const range = new Range({
      shortPattern: rangeSetting,
      forceUpperCase: Number.parseInt(setting.upper, 10) || false,
    }).toList();

    console.log(`range:${range}`);
    if (!isNaN(totalSetting)) {
      total = totalSetting;
    }

    const speaker = new Speaker({
      srcPrefix: AUDIO_HOST_PREFIX,
      repeat: repeatSetting,
    });

    const queue = new Queue({
      stage,
      stageHeight,
      range: range.length ? range : group1.split(''),
      total,
      forceUpperCase: Number.parseInt(setting.upper, 10) || false,
      speaker,
    });

    // 生成字母
    setInterval(function () {
      queue.spawn(fontBoundWidth);
    }, FREQUENCY);

    cax.tick(function () {
      if (isPageVisible) {
        return;
      } // tab 隐藏时，不再处理逻辑
      const speed = adjustSpeed(step, success, ratio);
      queue.senesce({
        step: speed,
      });
      stage.update();
    });

    // binding events
    document.onkeydown = function (e) {
      const charCode = e.which;
      const char = String.fromCharCode(charCode).toLowerCase();
      if (queue.isExist(char)) {
        speaker.read(char);
        queue.hide(char);
        success++;
      }
    };

    window.addEventListener('hashchange', () => {
      // todo: 读取新的参数
      window.location.reload();
    });

    document.addEventListener("visibilitychange", function () {
      isPageVisible = document.hidden;
    });
  }

  start(stage, FREQUENCY);
})();
